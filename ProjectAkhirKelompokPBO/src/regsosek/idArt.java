/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package regsosek;

/**
 */
public class idArt {

    public static void setText(String string) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    private String idAnggota;
    private String namaArt;
    private String nik;
    private String jenisKelamin;
    private String statusHub;
    private String umur;
    private String gangguan;
    private String jenisGangguan;
    private String ijazah;
    private String bekerja;
    private String lapanganUsaha;
    private String statusKedudukan;
    private String milikUsaha;
    private String jumlahUsaha;
    private String usahaUtama;

    public String getIdAnggota() {
        return idAnggota;
    }

    public void setIdAnggota(String idAnggota) {
        this.idAnggota = idAnggota;
    }
        public String getNamaArt() {
        return namaArt;
    }

    public void setNamaArt(String namaArt) {
        this.namaArt = namaArt;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getStatusHub() {
        return statusHub;
    }

    public void setStatusHub(String statusHub) {
        this.statusHub = statusHub;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getGangguan() {
        return gangguan;
    }

    public void setGangguan(String gangguan) {
        this.gangguan = gangguan;
    }

    public String getJenisGangguan() {
        return jenisGangguan;
    }

    public void setJenisGangguan(String jenisGangguan) {
        this.jenisGangguan = jenisGangguan;
    }

    public String getIjazah() {
        return ijazah;
    }

    public void setIjazah(String ijazah) {
        this.ijazah = ijazah;
    }

    public String getBekerja() {
        return bekerja;
    }

    public void setBekerja(String bekerja) {
        this.bekerja = bekerja;
    }

    public String getLapanganUsaha() {
        return lapanganUsaha;
    }

    public void setLapanganUsaha(String lapanganUsaha) {
        this.lapanganUsaha = lapanganUsaha;
    }

    public String getStatusKedudukan() {
        return statusKedudukan;
    }

    public void setStatusKedudukan(String statusKedudukan) {
        this.statusKedudukan = statusKedudukan;
    }

    public String getMilikUsaha() {
        return milikUsaha;
    }

    public void setMilikUsaha(String milikUsaha) {
        this.milikUsaha = milikUsaha;
    }

    
    public String getJumlahUsaha() {
        return jumlahUsaha;
    }

    public void setJumlahUsaha(String jumlahUsaha) {
        this.jumlahUsaha = jumlahUsaha;
    }

    
    public String getUsahaUtama() {
        return usahaUtama;
    }

    public void setUsahaUtama(String usahaUtama) {
        this.usahaUtama = usahaUtama;
    }

}
